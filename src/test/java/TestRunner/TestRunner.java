package TestRunner;


import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/main/resources/feature/Guru99.feature",
        glue = "co/com/poli/guru99/test/stepDefinition/"
)
public class TestRunner {

}
