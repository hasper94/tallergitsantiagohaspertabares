package co.com.poli.guru99.test.controller;

import co.com.poli.guru99.test.page.NewAccountPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class NewAccountController {
    WebDriver driver;
    Integer deposito;
    public void enterNewAccount(String consumer) throws InterruptedException {
        NewAccountPage newAccountPage = new NewAccountPage(driver);
        newAccountPage.clickInputCustomerId();
        newAccountPage.enterInputCustomerId(consumer);
        Thread.sleep(2000);
        newAccountPage.clickInputIniDeposit();
        newAccountPage.enterInputIniDeposit();
        newAccountPage.clickBtn2();

    }

    public NewAccountController(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public String getNewAccount(){
        NewAccountPage newAccountPage = new NewAccountPage(driver);
        return newAccountPage.getAccountValue();

    }

    public String getCurrentAmount (){
        NewAccountPage newAccountPage = new NewAccountPage(driver);
        return newAccountPage.getCurrentAmountValue();
    }

    public Integer depositValue (){
        NewAccountPage newAccountPage = new NewAccountPage(driver);
        return newAccountPage.depositValue();
    }
}
