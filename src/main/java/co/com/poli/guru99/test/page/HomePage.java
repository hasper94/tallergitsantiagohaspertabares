package co.com.poli.guru99.test.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
    private WebDriver driver;
    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Deposit')]")
    WebElement depositOption;
    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Balance Enquiry')]")
    WebElement balanceEnquiry;
    @FindBy(how = How.XPATH, using = "//a[contains(text(),'New Customer')]")
    WebElement newCustomerOption;
    @FindBy(how = How.XPATH, using = "//a[contains(text(),'New Account')]")
    WebElement newAccountOption;
    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Withdrawal')]")
    WebElement withdrawalOption;



    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public void depositOption () throws InterruptedException {
        Thread.sleep(2000);
        driver.get(depositOption.getAttribute("href"));
    }

    public void balanceEnquiryOption () throws InterruptedException {
        Thread.sleep(2000);
        driver.get(balanceEnquiry.getAttribute("href"));
    }

    public void newCustomerOption () throws InterruptedException {
        Thread.sleep(2000);
        driver.get(newCustomerOption.getAttribute("href"));
    }

    public void newAccountOption () throws InterruptedException {
        Thread.sleep(2000);
        driver.get(newAccountOption.getAttribute("href"));
    }

    public void withdrawalOption () throws InterruptedException {
        Thread.sleep(2000);
        driver.get(withdrawalOption.getAttribute("href"));
    }

}
