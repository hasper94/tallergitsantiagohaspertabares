package co.com.poli.guru99.test.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LoginPage  {

    private WebDriver driver;

    @FindBy(how = How.NAME, using = "uid")
    WebElement inputUserId;
    @FindBy(how = How.NAME, using = "password")
    WebElement inputPass;
    @FindBy(how = How.NAME, using ="btnLogin")
    WebElement btnLogin;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public  void clickUserId (){
        inputUserId.click();
    }

    public void enterUserId() {
        inputUserId.sendKeys("mngr264338");
    }

    public void clickPass(){
        inputPass.click();
    }

    public void enterPass(){
        inputPass.sendKeys("EqEbEgU");
    }

    public void clickBtnLogin(){
        btnLogin.click();
    }
}
