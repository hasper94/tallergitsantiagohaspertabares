package co.com.poli.guru99.test.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class BalanceEnquiryPage {
    private WebDriver driver;

    @FindBy(how = How.NAME, using = "accountno")
    WebElement inputAccountNo;
    @FindBy(how = How.NAME, using = "AccSubmit")
    WebElement btnAccountSubmit;
    @FindBy(how = How.XPATH, using = "/html[1]/body[1]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[16]/td[2]")
    WebElement balanceValue;


    public BalanceEnquiryPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public void clickInputAccountNo (){
        inputAccountNo.click();
    }

    public void enterInputAccountNo (String account){
        inputAccountNo.sendKeys(account);
    }

    public void clickBtnAccountSubmit (){
        btnAccountSubmit.click();
    }

    public String getBalanceValue(){
        return balanceValue.getText();
    }
}
