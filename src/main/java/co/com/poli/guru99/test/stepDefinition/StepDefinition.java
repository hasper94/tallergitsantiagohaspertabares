package co.com.poli.guru99.test.stepDefinition;

import co.com.poli.guru99.test.controller.GeneralController;
import co.com.poli.guru99.test.controller.LoginController;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class StepDefinition {
    public WebDriver driver;

    @Before
    public  void setUp (){
        System.setProperty("webdriver.chrome.driver", "./src/main/resources/drivers/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://demo.guru99.com/V4/index.php");
    }

    @After
    public void closeBrowser(){
        driver.close();
        driver.quit();
    }

    @Given("necesito loguearme en el portal guru")
    public void necesitoLoguearmeEnElPortalGuru() {
        LoginController loginController = new LoginController(driver);
        loginController.login();

    }

    @Given("necesito verificar la accion {string} con la opcion {string} con el campo {string}")
    public void necesitoVerificarLaAccionConLaOpcionConElCampo(String accion, String opcion, String campo) throws InterruptedException {
        GeneralController generalController = new GeneralController(driver);
        generalController.validation(accion,opcion,campo);

    }

    @When("realice la accion {string} y el {string}")
    public void realiceLaAccionYEl(String opcion, String campo) throws InterruptedException {
        GeneralController generalController = new GeneralController(driver);
        generalController.OptionValidation(opcion,campo);
    }


    @Then("el portal me muestra la informacion de la opcion {string} y el campo {string}")
    public void elPortalMeMuestraLaInformacionDeLaOpcionYElCampo(String opcion, String campo) throws InterruptedException {
        GeneralController generalController = new GeneralController(driver);
        generalController.TestValidation(opcion,campo);
    }
}
