package co.com.poli.guru99.test.controller;

import co.com.poli.guru99.test.page.NewCustomerPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class NewCustomerController {
    WebDriver driver;


    public void enterNewCustomer () throws InterruptedException {
        NewCustomerPage newCustomerPage = new NewCustomerPage(driver);
        newCustomerPage.clickInputName();
        newCustomerPage.enterInputName();
        newCustomerPage.clickInputFechaNacimiento();
        newCustomerPage.enterInputFechaNacimiento();
        newCustomerPage.clickInputAddress();
        newCustomerPage.enterInputAddress();
        newCustomerPage.clickInputCity();
        newCustomerPage.enterInputCity();
        newCustomerPage.clickInputState();
        newCustomerPage.enterInputState();
        newCustomerPage.clickInputPin();
        newCustomerPage.enterInputPin();
        newCustomerPage.clickInputTelephone();
        newCustomerPage.enterInputTelephone();
        newCustomerPage.clickInputEmail();
        newCustomerPage.enterInputEmail();
        newCustomerPage.clickInputPassword();
        newCustomerPage.enterInputPassword();
        newCustomerPage.clickBtnSubmit();
    }

    public NewCustomerController(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public String getNewCustomer(){
        NewCustomerPage newCustomerPage = new NewCustomerPage(driver);
        return newCustomerPage.getAccountValue();
    }
}
