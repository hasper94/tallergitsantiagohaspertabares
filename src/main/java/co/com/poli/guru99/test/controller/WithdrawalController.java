package co.com.poli.guru99.test.controller;

import co.com.poli.guru99.test.page.HomePage;
import co.com.poli.guru99.test.page.WithdrawalPage;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class WithdrawalController {

    WebDriver driver;
    String customer;
    String account;
    Integer currentAmount;
    public String newBalanceAfterWithdrawal;
    Integer retiroValue;
    Integer balIntroPrueba;
    Integer retIntroCom;
    Integer InitialDeposit;

    public void enterWithdrawal (String accountId){
        WithdrawalPage withdrawalPage = new WithdrawalPage(driver);
        withdrawalPage.clickInputAccountId();
        withdrawalPage.enterInputAccountId(accountId);
        withdrawalPage.clickInputAmount();
        withdrawalPage.enterInputAmount();
        withdrawalPage.clickInputDescription();
        withdrawalPage.enterInputDescription();
        withdrawalPage.clickbtnAccSubmit();


    }

    public void validateOption () throws InterruptedException {
        System.out.println("Caso de prueba 3: Verificar que al realizar un retiro (opción Withdrawal), el saldo de la cuenta sea afectado.");
        //primero se crea customer
        Thread.sleep(2000);
        NewCustomerController newCustomerController = new NewCustomerController(driver);
        newCustomerController.enterNewCustomer();
        newCustomerController.getNewCustomer();
        Thread.sleep(2000);
        customer = newCustomerController.getNewCustomer();
        System.out.println(customer);
        Thread.sleep(2000);

        HomePage homePage = new HomePage(driver);
        homePage.newAccountOption();
        Thread.sleep(2000);
        NewAccountController newAccountController =  new NewAccountController(driver);
        newAccountController.enterNewAccount(customer);
        account = newAccountController.getNewAccount();

        Thread.sleep(2000);

        homePage.withdrawalOption();
        Thread.sleep(2000);

        WithdrawalController withdrawalController = new WithdrawalController(driver);
        withdrawalController.enterWithdrawal(account);
        Thread.sleep(2000);

        newBalanceAfterWithdrawal= withdrawalController.getCurrentBalanceWithdrawal();

        Thread.sleep(2000);
    }

    public void TestValidation () throws InterruptedException {
        Thread.sleep(2000);
        NewAccountController newAccountController =  new NewAccountController(driver);
        currentAmount = newAccountController.depositValue();

        WithdrawalController withdrawalController = new WithdrawalController(driver);
        retiroValue = withdrawalController.WithdrawalValue();

        balIntroPrueba = currentAmount-retiroValue;

        WithdrawalPage withdrawalPage =  new WithdrawalPage(driver);

        retIntroCom = Integer.parseInt(withdrawalPage.getCurrentBalanceWithdrawal());

        Thread.sleep(2000);
        Assert.assertEquals(balIntroPrueba, retIntroCom);
    }

    public Integer WithdrawalValue(){
        WithdrawalPage withdrawalPage = new WithdrawalPage(driver);
        return withdrawalPage.WithdrawalValue();
    }

    public WithdrawalController(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public String getCurrentBalanceWithdrawal (){
        WithdrawalPage withdrawalPage = new WithdrawalPage(driver);
        return withdrawalPage.getCurrentBalanceWithdrawal();
    }
}
