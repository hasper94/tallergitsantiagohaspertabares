package co.com.poli.guru99.test.controller;

import co.com.poli.guru99.test.page.HomePage;
import org.openqa.selenium.WebDriver;

public class HomeController {
    WebDriver driver;

    public void validateOption (String opcion,String campo) throws InterruptedException {
        HomePage homePage = new HomePage(driver);
        if(opcion.equalsIgnoreCase("Deposit")&&campo.equalsIgnoreCase("Amount")){
            /*
             * Este es el inicio del cambio
             * */
            homePage.withdrawalOption();
            /*
             * Este es el fin del cambio
             * */

//si, buenas noches

            //Esta es una linea para guardarla temporalmente (Stash)

            homePage.depositOption();
        }else{
            homePage.newCustomerOption();
        }

    }

    public HomeController(WebDriver driver) {
        this.driver = driver;
    }
}
