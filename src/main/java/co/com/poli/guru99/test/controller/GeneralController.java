package co.com.poli.guru99.test.controller;

import org.openqa.selenium.WebDriver;

public class GeneralController {
    WebDriver driver;
    public void validation(String accion, String opcion, String campo) throws InterruptedException {

        System.out.println(accion+" "+opcion+" "+campo);
        HomeController homeController = new HomeController(driver);

        homeController.validateOption(opcion,campo);

    }

    public void OptionValidation (String opcion, String campo) throws InterruptedException {
        if(opcion.equalsIgnoreCase("Deposit")){
            DepositController depositController = new DepositController(driver);
            depositController.validateOption(opcion,campo);
        }else{
            WithdrawalController withdrawalController = new WithdrawalController(driver);
            withdrawalController.validateOption();
        }



    }

    public void TestValidation(String opcion, String campo) throws InterruptedException {
        if(opcion.equalsIgnoreCase("Deposit")){
            DepositController depositController = new DepositController(driver);
            depositController.TestValidation(opcion,campo);
        }else{
            WithdrawalController withdrawalController = new WithdrawalController(driver);
            withdrawalController.TestValidation();
        }

    }

    public GeneralController(WebDriver driver) {
        this.driver = driver;
    }
}
