package co.com.poli.guru99.test.controller;

import co.com.poli.guru99.test.page.DepositPage;
import co.com.poli.guru99.test.page.HomePage;
import co.com.poli.guru99.test.page.NewAccountPage;
import org.junit.Assert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;

public class DepositController {
    WebDriver driver;
    String balance1 ;
    //String balance2 = "";
    String customer;
    String account;
    Integer inputAmount;
    Integer newbalance;
    Integer suma;
    String currentBalance;
    Integer InitialDeposit;

    public void validateOption (String opcion,String campo) throws InterruptedException {

        if(opcion.equalsIgnoreCase("Deposit")&&campo.equalsIgnoreCase("Amount")){
            System.out.println("Caso de prueba 1: Verificar que al realizar deposito (opción Deposit), el campo Amount sea obligatorio.");
            DepositPage depositPage = new DepositPage(driver);
            depositPage.clickInputAccountNo();
            depositPage.enterInputAccountNo();
            depositPage.clickInputDescription();
            depositPage.enterInputDescription();
        }
        else{
            System.out.println("Caso de prueba 2: Verificar que al realizar un deposito (opción Deposit)," +
                    " el saldo de la cuenta si se actualice con el nuevo valor. Para ello, debe crear un customer, luego una account.");
            //primero se crea customer
            Thread.sleep(2000);
            NewCustomerController newCustomerController = new NewCustomerController(driver);
            newCustomerController.enterNewCustomer();
            newCustomerController.getNewCustomer();
            Thread.sleep(2000);
            customer = newCustomerController.getNewCustomer();
            System.out.println(customer);
            Thread.sleep(2000);

            HomePage homePage = new HomePage(driver);
            homePage.newAccountOption();
            Thread.sleep(2000);
            NewAccountController newAccountController =  new NewAccountController(driver);
            newAccountController.enterNewAccount(customer);
            account = newAccountController.getNewAccount();

            System.out.println(account);
            Thread.sleep(2000);

            homePage.balanceEnquiryOption();
            Thread.sleep(2000);
            BalanceEnquiryController balanceEnquiryController = new BalanceEnquiryController(driver);
            balanceEnquiryController.enterAccount(account);
            balance1 = balanceEnquiryController.getBalanceValue();
            Thread.sleep(2000);
            System.out.println("Este es el balance nuevo: "+balance1);
            Thread.sleep(2000);
            homePage.depositOption();
            Thread.sleep(2000);

            DepositPage depositPage = new DepositPage(driver);
            depositPage.clickInputAccountNo();
            depositPage.enterInputAccountNo();
            depositPage.clickInputAmount();
            depositPage.enterInputAmount();
            depositPage.clickInputDescription();
            depositPage.enterInputDescription();
            depositPage.clickBtnSubmit();
            Thread.sleep(2000);

        }


    }

    public void TestValidation (String opcion,String campo) throws InterruptedException {

        if(opcion.equalsIgnoreCase("Deposit")&&campo.equalsIgnoreCase("Amount")){
            DepositPage depositPage = new DepositPage(driver);
            try {
                depositPage.clickBtnSubmit();
                Thread.sleep(2000);
                depositPage.getAlertAmount();
            } catch (UnhandledAlertException f) {
                try {
                    Thread.sleep(2000);
                    depositPage.getAlertAmount();
                } catch (NoAlertPresentException e) {
                    e.printStackTrace();
                }
            }
        }else {
            Thread.sleep(2000);
            DepositPage depositPage = new DepositPage(driver);
            inputAmount = depositPage.depostiValue();
            Thread.sleep(2000);
            NewAccountPage newAccountPage = new NewAccountPage(driver);
            InitialDeposit = newAccountPage.depositValue();

            currentBalance = depositPage.getCurrentBalance();
            newbalance = Integer.parseInt(currentBalance);
            suma = InitialDeposit+inputAmount;
            Thread.sleep(2000);
            Assert.assertEquals(suma, newbalance);

        }
    }
    public DepositController(WebDriver driver) {
        this.driver = driver;
    }
}
