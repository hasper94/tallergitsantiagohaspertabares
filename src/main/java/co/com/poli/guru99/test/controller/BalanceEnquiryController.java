package co.com.poli.guru99.test.controller;

import co.com.poli.guru99.test.page.BalanceEnquiryPage;
import org.openqa.selenium.WebDriver;

public class BalanceEnquiryController {
    WebDriver driver;

    public void enterAccount(String account){
        BalanceEnquiryPage balanceEnquiryPage = new BalanceEnquiryPage(driver);
        balanceEnquiryPage.clickInputAccountNo();
        balanceEnquiryPage.enterInputAccountNo(account);
        balanceEnquiryPage.clickBtnAccountSubmit();
    }

    public String getBalanceValue(){
        BalanceEnquiryPage balanceEnquiryPage = new BalanceEnquiryPage(driver);
        return balanceEnquiryPage.getBalanceValue();
    }

    public BalanceEnquiryController(WebDriver driver) {
        this.driver = driver;
    }
}
