package co.com.poli.guru99.test.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class WithdrawalPage {
    private WebDriver driver;
    Integer retiroPrueba = 3200;

    @FindBy(how = How.NAME, using = "accountno")
    WebElement inputAccountId;
    @FindBy(how = How.NAME, using = "ammount")
    WebElement inputAmount;
    @FindBy(how = How.NAME, using = "desc")
    WebElement inputDescription;
    @FindBy(how = How.NAME, using = "AccSubmit")
    WebElement btnAccSubmit;
    @FindBy(how = How.XPATH, using = "/html[1]/body[1]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[23]/td[2]")
    WebElement currentBalance;


    public String getCurrentBalanceWithdrawal (){
        return currentBalance.getText();
    }

    public WithdrawalPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public void clickInputAccountId (){
        inputAccountId.click();
    }

    public void enterInputAccountId (String accountId){
        inputAccountId.sendKeys(accountId);
    }

    public void clickInputAmount (){
        inputAmount.click();
    }

    public void enterInputAmount (){
        inputAmount.sendKeys(Integer.toString(retiroPrueba));

    }

    public void clickInputDescription (){
        inputDescription.click();
    }
    public void enterInputDescription (){
        inputDescription.sendKeys("retiro prueba");
    }

    public void clickbtnAccSubmit (){
        btnAccSubmit.click();
    }

    public Integer WithdrawalValue(){
        return retiroPrueba;
    }

}
