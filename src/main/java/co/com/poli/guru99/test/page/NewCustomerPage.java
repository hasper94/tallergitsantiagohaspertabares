package co.com.poli.guru99.test.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.Random;

public class NewCustomerPage {
    private WebDriver driver;

    @FindBy(how = How.NAME, using = "name")
    WebElement inputName;
    @FindBy(how = How.ID, using = "dob")
    WebElement inputFechaNacimiento;
    @FindBy(how = How.NAME, using = "addr")
    WebElement inputAddress;
    @FindBy(how = How.NAME, using = "city")
    WebElement inputCity;
    @FindBy(how = How.NAME, using = "state")
    WebElement inputState;
    @FindBy(how = How.NAME, using = "pinno")
    WebElement inputPin;
    @FindBy(how = How.NAME, using = "telephoneno")
    WebElement inputTelephone;
    @FindBy(how = How.NAME, using = "emailid")
    WebElement inputEmail;
    @FindBy(how = How.NAME, using = "password")
    WebElement inputPassword;
    @FindBy(how = How.NAME, using = "sub")
    WebElement btnSubmit;
    @FindBy(how = How.XPATH, using = "/html[1]/body[1]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[4]/td[2]")
    WebElement accountValue;

    public String getAccountValue(){
        return accountValue.getText();
    }

    //constructor
    public NewCustomerPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public void clickInputName (){
        inputName.click();
    }

    public void enterInputName (){
        inputName.sendKeys("Nombre Prueba");
    }

    public void clickInputFechaNacimiento (){
        inputFechaNacimiento.click();
    }

    public void enterInputFechaNacimiento (){
        inputFechaNacimiento.clear();
        inputFechaNacimiento.sendKeys("01\n04\n1994");
    }

    public void clickInputAddress (){
        inputAddress.click();
    }

    public void enterInputAddress (){
        inputAddress.sendKeys("Calle falsa");
    }

    public void clickInputCity (){
        inputCity.click();
    }

    public void enterInputCity (){
        inputCity.sendKeys("ciudad prueba");
    }

    public void clickInputState (){
        inputState.click();
    }

    public void enterInputState (){
        inputState.sendKeys("estado de prueba");
    }

    public void clickInputPin (){
        inputPin.click();
    }

    public void enterInputPin (){
        inputPin.sendKeys("123456");
    }

    public void clickInputTelephone (){
        inputTelephone.click();
    }

    public void enterInputTelephone (){
        inputTelephone.sendKeys("6062528");
    }

    public void clickInputEmail (){
        inputEmail.click();
    }

    public void enterInputEmail (){
        int nombre = 0;
        Random rnd1 = new Random();
        nombre = rnd1.nextInt();

        inputEmail.sendKeys("prueba"+nombre+"@prueba.com");
    }


    public void clickInputPassword (){
        inputPassword.click();
    }

    public void enterInputPassword (){
        inputPassword.sendKeys("admin123");
    }

    public void clickBtnSubmit (){
        btnSubmit.click();
    }


}
