package co.com.poli.guru99.test.page;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class DepositPage {

    private WebDriver driver;
    String mensajeError;
    Integer deposit = 5000;

    @FindBy(how = How.NAME, using = "accountno")
    WebElement inputAccountNo;
    @FindBy(how = How.NAME, using = "desc")
    WebElement inputDescription;
    @FindBy(how = How.NAME, using = "AccSubmit")
    WebElement btnSubmit;
    @FindBy(how =  How.XPATH, using = "/html[1]/body[1]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[7]/td[2]/input[1]")
    WebElement inputAmount;
    @FindBy(how =  How.XPATH, using = "/html[1]/body[1]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[23]/td[2]")
    WebElement currentBalance;




    public DepositPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public String getCurrentBalance (){
        return currentBalance.getText();
    }

    public void clickInputAccountNo (){
        inputAccountNo.click();
    }

    public void clickInputAmount(){
        inputAmount.click();
    }

    public void enterInputAmount(){
        inputAmount.sendKeys(Integer.toString(deposit));
    }

    public String getAmountValue (){
        return inputAmount.getText();
    }

    public void enterInputAccountNo (){
        inputAccountNo.sendKeys("79483");
    }

    public void clickInputDescription(){
        inputDescription.click();
    }

    public void enterInputDescription (){
        inputDescription.sendKeys("prueba123");
    }

    public void clickBtnSubmit (){
        btnSubmit.click();
    }

    public Integer depostiValue(){
        return deposit;
    }

    public void getAlertAmount () throws InterruptedException {

        String confirmacion="Please fill all fields";
        Alert alert = driver.switchTo().alert();
        String alertMessage= driver.switchTo().alert().getText();
        // Displaying alert message
        System.out.println(alertMessage);
        Thread.sleep(2000);

        // Accepting alert
        alert.accept();
       Assert.assertTrue(alertMessage.contains(confirmacion));

    }

}
