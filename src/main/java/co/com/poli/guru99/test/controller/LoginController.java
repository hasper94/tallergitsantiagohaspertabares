package co.com.poli.guru99.test.controller;

import co.com.poli.guru99.test.page.LoginPage;
import org.openqa.selenium.WebDriver;

public class LoginController {
    WebDriver driver;

    public void login (){
        LoginPage loginPage = new LoginPage(driver);
        loginPage.clickUserId();
        loginPage.enterUserId();
        loginPage.clickPass();
        loginPage.enterPass();
        loginPage.clickBtnLogin();
    }

    public LoginController(WebDriver driver) {
        this.driver = driver;
    }
}
