package co.com.poli.guru99.test.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class NewAccountPage {

    private WebDriver driver;
    Integer InitialDepositValue = 5000;

    @FindBy(how = How.NAME, using = "cusid")
    WebElement inputCustomerId;
    @FindBy(how = How.NAME, using = "inideposit")
    WebElement inputIniDeposit;
    @FindBy(how = How.NAME, using = "button2")
    WebElement btn2;
    @FindBy(how = How.XPATH, using = "/html[1]/body[1]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[4]/td[2]")
    WebElement accountValue;
    @FindBy(how = How.XPATH, using = " /html[1]/body[1]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[10]/td[2]")
    WebElement currentAmountValue;



    public String getAccountValue(){
        return accountValue.getText();
    }

    public NewAccountPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public void clickInputCustomerId (){
        inputCustomerId.click();
    }

    public String getCurrentAmountValue(){
        return currentAmountValue.getText();
    }

    public void enterInputCustomerId (String customer){
        inputCustomerId.sendKeys(customer);
    }

    public void clickInputIniDeposit (){
        inputIniDeposit.click();
    }

    public Integer enterInputIniDeposit (){
        inputIniDeposit.sendKeys(Integer.toString(InitialDepositValue));
        return InitialDepositValue;
    }

    public Integer depositValue (){
        return InitialDepositValue;
    }

    public void clickBtn2 (){
        btn2.click();
    }

}
