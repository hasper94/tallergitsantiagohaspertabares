Feature:  Verificar diferentes opciones del portal guru99

  Background: Loguearse
    Given necesito loguearme en el portal guru


 Scenario Outline: Verificar opciones (Deposit,Withdrawal)
    Given necesito verificar la accion "<accion>" con la opcion "<opcion>" con el campo "<campo>"
    When realice la accion "<opcion>" y el "<campo>"
    Then el portal me muestra la informacion de la opcion "<opcion>" y el campo "<campo>"

   Examples:
   |accion|opcion|campo|
   |deposito|Deposit|Amount|
   |deposito|Deposit|Saldo|
   |retiro  |Withdrawal|Saldo|